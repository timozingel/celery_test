import json
from datetime import timedelta

from celery.schedules import crontab
from celery import Celery
from flask import Flask

def make_celery(app):
    celery = Celery(app.import_name, backend=app.config['CELERY_RESULT_BACKEND'],
                    broker=app.config['CELERY_BROKER_URL'])
    celery.conf.update(app.config)
    TaskBase = celery.Task
    # add flask app context to celery tasks
    class ContextTask(TaskBase):
        abstract = True
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)
    celery.Task = ContextTask
    return celery


app = Flask(__name__)
content_type_json = {'Content-Type': 'text/css; charset=utf-8'}

# Celery conf
app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'
#app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379/0'
app.config['CELERY_RESULT_BACKEND'] = None
app.config['CELERY_TIMEZONE'] = 'UTC'

# execute task at certain intervals
app.config['CELERYBEAT_SCHEDULE'] = {
    'play-every-morning': {
        'task': 'tasks.play_task',
        'schedule': crontab(hour=9, minute=0)
    },
    'pause-later': {
        'task': 'tasks.pause_task',
        'schedule': crontab(hour=9, minute=10)
    }
}

celery = make_celery(app)


# Routes for manual controls
############################
import tasks

@app.route('/')
def hello_world():
    msg = 'Device: <a href="/play">play</a> or <a href="/pause">pause</a>.'
    return msg

@app.route('/play')
def get_play():
    tasks.play_task.delay()
    return 'Playinasfdasfd <a href="/">back</a>'

@app.route('/pause')
def get_pause():
    tasks.pause_task.delay()
    return 'Pausing! <a href="/">back</a>'

if __name__ == '__main__':
    try:
        # try the production run
        app.run(host='0.0.0.0', port=80)
    except PermissionError:
        # we're probably on the developer's machine
        app.run(host='0.0.0.0', port=5000, debug=True)
